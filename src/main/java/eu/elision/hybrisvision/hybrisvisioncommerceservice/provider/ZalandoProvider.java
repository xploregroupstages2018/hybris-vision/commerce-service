package eu.elision.hybrisvision.hybrisvisioncommerceservice.provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Product;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.ProductSearch;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.VisionInfo;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.exception.ProductFetchException;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.util.JsonUtil;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.util.ParameterUtil;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.util.TagUtil;
import eu.elision.hybrisvision.util.CombinationsUtil;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@Service(value = Provider.Constants.ZALANDO)
public class ZalandoProvider implements ProductProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZalandoProvider.class);

    private static final String BASE_URL = "https://www.zalando.co.uk";
    private static final String ARTICLES_URL = BASE_URL + "/api/catalog/articles";
    private static final String TAG_TYPE_LABEL = "label";
    private static final String TAG_TYPE_COLOR = "color";
    private static final String TAG_TYPE_FILTER = "filter";
    private static final String REQUEST_METHOD_GET = "GET";
    private static final String RESPONSE_ARTICLES = "articles";

    @Override
    public ProductSearch getProducts(List<Tag> keywords) {

        List<Tag> labelParameters = TagUtil.createParameterList(keywords, TAG_TYPE_LABEL);
        List<Tag> colorParameters = TagUtil.createParameterList(keywords, TAG_TYPE_COLOR);
        List<Tag> genderParameters = TagUtil.createParameterList(keywords, TAG_TYPE_FILTER);

        labelParameters.sort((o1, o2) -> Float.compare(o2.getImportance(), o1.getImportance()));
        System.out.println(labelParameters);
        VisionInfo visionInfo = new VisionInfo(labelParameters, colorParameters, genderParameters);

        return getResult(visionInfo, labelParameters.size());
    }

    private ProductSearch getResult(VisionInfo visionInfo, int size) {

        for (int i = size; i > 0; i--) {

            List<List<Tag>> combinations = CombinationsUtil.combination(visionInfo.getLabelParameters(), i);
            ProductSearch productSearch = checkCombinations(visionInfo, combinations);
            if (productSearch != null && !productSearch.getProducts().isEmpty()) {
                return productSearch;
            }
        }
        return null;
    }

    private ProductSearch checkCombinations(VisionInfo visionInfo, List<List<Tag>> tagCombinations) {
        for (List<Tag> tagCombo : tagCombinations) {
            String params = createParameterString(visionInfo, tagCombo);
            String finalUrl = ARTICLES_URL + params;
            List<Product> products = fetchProducts(finalUrl);
            if (!products.isEmpty()) {
                return buildProductSearch(products, tagCombo);
            }
        }

        return null;
    }

    private ProductSearch buildProductSearch(List<Product> productsFound, List<Tag> tagsUsed) {
        ProductSearch productSearch = new ProductSearch();
        productSearch.setProducts(productsFound);
        productSearch.setTags(tagsUsed);
        return productSearch;
    }

    private List<Product> fetchProducts(String url) {

        List<Product> products = new ArrayList<>();

        try {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(REQUEST_METHOD_GET);

            LOGGER.info("Calling: " + url);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String response = getResponse(in);

            JSONArray jsonArray = JsonUtil.getArray(response, RESPONSE_ARTICLES);
            fillProductList(jsonArray, products);
            in.close();

        } catch (IOException e) {
            throw new ProductFetchException("Something went wrong while trying to fetch the products");
        } catch (ParseException e) {
            throw new ProductFetchException("Something went wrong while trying to parse the products");
        }

        return products;
    }

    private void fillProductList(JSONArray jsonArray, List<Product> products) throws IOException {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        for (Object object : jsonArray) {
            products.add(mapper.readValue(object.toString(), Product.class));
        }
    }

    private String getResponse(BufferedReader in) throws IOException {
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        return response.toString();
    }

    private String createParameterString(VisionInfo visionInfo, List<Tag> tagCombo) {

        StringBuilder labelQuery = ParameterUtil.buildLabelQuery(tagCombo);
        StringBuilder colorQuery = ParameterUtil.buildColorQuery(visionInfo.getColorParameters());
        StringBuilder genderQuery = ParameterUtil.buildGenderQuery(visionInfo.getGenderParameters());

        String queryString = cutQueryString(colorQuery, labelQuery);
        String colorString = cutColorString(colorQuery);
        System.out.println(buildParameterString(queryString, colorString, genderQuery.toString()));
        return buildParameterString(queryString, colorString, genderQuery.toString());
    }

    private String buildParameterString(String labelQuery, String colorQuery, String genderQuery) {

        return labelQuery.replace(" ", "").replace("&#45", "")
                + colorQuery + genderQuery;
    }

    private String cutColorString(StringBuilder colors) {

        String colorString;
        if (colors.length() <= 1) {
            colorString = "";
        } else {
            colorString = colors.toString().substring(0, colors.length() - 1);
        }
        return colorString;
    }

    private String cutQueryString(StringBuilder colors, StringBuilder query) {

        String queryString;
        if (query.length() <= 7) {
            queryString = query.toString().substring(0, 1);
        } else {
            colors.insert(0, "&");
            queryString = query.toString().substring(0, query.toString().length() - 3);
        }
        return queryString;
    }
}
