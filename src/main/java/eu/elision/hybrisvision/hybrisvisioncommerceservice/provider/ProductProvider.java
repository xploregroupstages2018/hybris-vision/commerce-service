package eu.elision.hybrisvision.hybrisvisioncommerceservice.provider;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.ProductSearch;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;

import java.util.List;

public interface ProductProvider {

    ProductSearch getProducts(List<Tag> keywords);
}
