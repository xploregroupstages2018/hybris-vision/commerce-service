package eu.elision.hybrisvision.hybrisvisioncommerceservice.locator;


import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.ProductProvider;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;

public interface ProviderLocator {

    ProductProvider getInstance(Provider provider);
}
