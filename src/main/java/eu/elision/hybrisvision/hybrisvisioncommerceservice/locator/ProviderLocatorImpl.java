package eu.elision.hybrisvision.hybrisvisioncommerceservice.locator;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.ProductProvider;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ProviderLocatorImpl implements ProviderLocator {

    @Autowired
    private Map<String, ProductProvider> providerRegistry;

    public ProductProvider getInstance(Provider provider) {
        return providerRegistry.get(provider.getValue());
    }
}
