package eu.elision.hybrisvision.hybrisvisioncommerceservice.converter;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.exception.ProviderException;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;

import java.beans.PropertyEditorSupport;

public class ProviderConverter extends PropertyEditorSupport {

    /*
     * @inheritDoc
     */
    @Override
    public void setAsText(String text) {
        try {
            setValue(Provider.fromValue(text));
        } catch (IllegalArgumentException e) {
            throw new ProviderException(e.getMessage());
        }
    }
}