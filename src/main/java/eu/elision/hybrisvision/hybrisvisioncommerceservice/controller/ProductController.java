package eu.elision.hybrisvision.hybrisvisioncommerceservice.controller;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.converter.ProviderConverter;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.ProductSearch;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Response;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.exception.ParameterException;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * @author Lander
 * @since 14/05/2018
 */
@RestController
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @CrossOrigin
    @PostMapping(value = "/product", produces = {"application/json"})
    public ResponseEntity getProducts(@RequestParam(value = "provider") Provider provider,
                                      @RequestBody(required = false) Tag[] keywords) {

        if (keywords == null) {
            throw new ParameterException("No keywords found");
        }

        ProductSearch productSearch = productService.getProducts(provider, Arrays.asList(keywords));
        if (productSearch != null) {
            Response response = new Response();
            response.setProvider(provider.getValue());
            response.setTags(productSearch.getTags());
            response.setProducts(productSearch.getProducts());
            return ResponseEntity.ok().body(response);
        }

        return ResponseEntity.notFound().build();
    }

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(Provider.class, new ProviderConverter());
    }
}
