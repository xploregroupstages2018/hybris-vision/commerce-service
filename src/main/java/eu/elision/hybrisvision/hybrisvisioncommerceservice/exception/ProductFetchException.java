package eu.elision.hybrisvision.hybrisvisioncommerceservice.exception;

public class ProductFetchException extends RuntimeException {

    public ProductFetchException(String message) {
        super(message);
    }
}
