package eu.elision.hybrisvision.hybrisvisioncommerceservice.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = ErrorConstant.INVALID_PROVIDER)
    @ExceptionHandler(value = {ProviderException.class})
    public void handleIssue(ProviderException e) {
        LOGGER.error(e.getLocalizedMessage(), e);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = ErrorConstant.NO_KEYWORDS_FOUND)
    @ExceptionHandler(value = {ParameterException.class})
    public void handleIssue(ParameterException e) {
        LOGGER.error(e.getLocalizedMessage(), e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = ErrorConstant.FETCH_ERROR)
    @ExceptionHandler(value = {ProductFetchException.class})
    public void handleIssue(ProductFetchException e) {
        LOGGER.error(e.getLocalizedMessage(), e);
    }

}
