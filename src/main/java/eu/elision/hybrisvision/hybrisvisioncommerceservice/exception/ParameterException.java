package eu.elision.hybrisvision.hybrisvisioncommerceservice.exception;

public class ParameterException extends RuntimeException {

    public ParameterException(String message) {
        super(message);
    }
}
