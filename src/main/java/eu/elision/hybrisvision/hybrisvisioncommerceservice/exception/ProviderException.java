package eu.elision.hybrisvision.hybrisvisioncommerceservice.exception;

public class ProviderException extends RuntimeException {

    public ProviderException(String message) {
        super(message);
    }
}