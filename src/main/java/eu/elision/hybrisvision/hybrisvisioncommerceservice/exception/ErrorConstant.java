package eu.elision.hybrisvision.hybrisvisioncommerceservice.exception;

public class ErrorConstant {

    public static final String DEFAULT = "Something went wrong while fetching products";
    public static final String INVALID_PROVIDER = "Invalid provided";
    public static final String NO_KEYWORDS_FOUND = "No keywords found";
    public static final String FETCH_ERROR = "Something went wrong while trying to fetch the products";
}