package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Lander
 * @since 15/05/2018
 */
public class Price {
    private String original;

    private String promotional;

    @JsonProperty("has_different_prices")
    private boolean hasDifferentPrices;

    @JsonProperty("has_discount_on_selected_sizes_only")
    private boolean hasDiscountOnSelectedSizesOnly;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getPromotional() {
        return promotional;
    }

    public void setPromotional(String promotional) {
        this.promotional = promotional;
    }

    public boolean isHasDifferentPrices() {
        return hasDifferentPrices;
    }

    public void setHasDifferentPrices(boolean hasDifferentPrices) {
        this.hasDifferentPrices = hasDifferentPrices;
    }

    public boolean isHasDiscountOnSelectedSizesOnly() {
        return hasDiscountOnSelectedSizesOnly;
    }

    public void setHasDiscountOnSelectedSizesOnly(boolean hasDiscountOnSelectedSizesOnly) {
        this.hasDiscountOnSelectedSizesOnly = hasDiscountOnSelectedSizesOnly;
    }
}
