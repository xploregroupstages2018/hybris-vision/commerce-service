package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Lander
 * @since 15/05/2018
 */
public class Media {

    private String path;

    @JsonProperty("packet_shot")
    private boolean packetShot;

    private String role;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isPacketShot() {
        return packetShot;
    }

    public void setPacketShot(boolean packetShot) {
        this.packetShot = packetShot;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
