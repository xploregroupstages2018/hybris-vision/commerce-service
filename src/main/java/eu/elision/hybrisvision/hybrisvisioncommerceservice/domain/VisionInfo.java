package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import java.util.List;

public class VisionInfo {

    private List<Tag> labelParameters;
    private List<Tag> colorParameters;
    private List<Tag> genderParameters;

    public VisionInfo(List<Tag> labelParameters, List<Tag> colorParameters, List<Tag> genderParameters) {
        this.labelParameters = labelParameters;
        this.colorParameters = colorParameters;
        this.genderParameters = genderParameters;
    }

    public List<Tag> getLabelParameters() {
        return labelParameters;
    }

    public List<Tag> getColorParameters() {
        return colorParameters;
    }

    public List<Tag> getGenderParameters() {
        return genderParameters;
    }
}
