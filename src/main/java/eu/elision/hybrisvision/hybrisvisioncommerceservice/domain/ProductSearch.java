package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import java.util.List;

public class ProductSearch {

    private List<Product> products;
    private List<Tag> tags;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
