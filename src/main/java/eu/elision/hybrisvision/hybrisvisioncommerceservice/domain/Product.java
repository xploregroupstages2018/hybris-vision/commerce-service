package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

/**
 * @author Lander
 * @since 14/05/2018
 */
public class Product {

    private String sku;
    private String name;
    private Price price;
    private String[] sizes;

    @JsonProperty("url_key")
    private String urlKey;
    private Media[] media;

    @JsonProperty("brand_name")
    private String brandName;

    @JsonProperty("family_articles")
    private Product[] familyArticles;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String[] getSizes() {
        return sizes;
    }

    public void setSizes(String[] sizes) {
        this.sizes = sizes;
    }

    public String getUrlKey() {
        return urlKey;
    }

    public void setUrlKey(String urlKey) {
        this.urlKey = urlKey;
    }

    public Media[] getMedia() {
        return media;
    }

    public void setMedia(Media[] media) {
        this.media = media;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Product[] getFamilyArticles() {
        return familyArticles;
    }

    public void setFamilyArticles(Product[] familyArticles) {
        this.familyArticles = familyArticles;
    }

    @Override
    public String toString() {
        return "Product{" +
                "sku='" + sku + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", sizes=" + Arrays.toString(sizes) +
                ", urlKey='" + urlKey + '\'' +
                ", media=" + Arrays.toString(media) +
                ", brandName='" + brandName + '\'' +
                ", familyArticles=" + Arrays.toString(familyArticles) +
                '}';
    }
}
