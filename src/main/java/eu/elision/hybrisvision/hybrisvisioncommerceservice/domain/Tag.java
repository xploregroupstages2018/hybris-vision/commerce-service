package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

/**
 * @author Lander
 * @since 16/05/2018
 */
public class Tag {

    private String text;
    private boolean enabled;
    private String type;
    private float importance;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getImportance() {
        return importance;
    }

    public void setImportance(float importance) {
        this.importance = importance;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "text='" + text + '\'' +
                ", enabled=" + enabled +
                ", type='" + type + '\'' +
                ", importance=" + importance +
                '}';
    }
}
