package eu.elision.hybrisvision.hybrisvisioncommerceservice.domain;

import java.util.List;

public class Response {

    private String provider;
    private List<Product> products;
    private List<Tag> tags;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
