package eu.elision.hybrisvision.hybrisvisioncommerceservice.util;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;

import java.util.List;

public class ParameterUtil {

    private static final String CHARACTER_SPACE = "%20";
    private static final String CHARACTER_OTHER_PARAM = "&";
    private static final String CHARACTER_START_PARAM = "?";

    public static StringBuilder buildGenderQuery(List<Tag> genderParameters) {

        StringBuilder genderQueryBuilder = new StringBuilder();
        genderParameters.forEach(tag -> genderQueryBuilder.append(CHARACTER_OTHER_PARAM).append("categories=").append(tag.getText().toLowerCase()));
        return genderQueryBuilder;
    }

    public static StringBuilder buildColorQuery(List<Tag> colorList) {

        StringBuilder colorQueryBuilder = new StringBuilder();

        colorList.forEach(k -> {
            if (k.isEnabled()) {
                colorQueryBuilder.append("colors=").append(k.getText().toUpperCase()).append(CHARACTER_OTHER_PARAM);
            }
        });

        return colorQueryBuilder;
    }

    public static StringBuilder buildLabelQuery(List<Tag> keywords) {

        StringBuilder keywordQueryBuilder = new StringBuilder();

        keywordQueryBuilder.append(CHARACTER_START_PARAM);
        keywordQueryBuilder.append("query=");
        keywords.forEach(keyword -> {
            if (keyword.isEnabled()) {
                keywordQueryBuilder.append(keyword.getText()).append(CHARACTER_SPACE);
            }
        });

        return keywordQueryBuilder;
    }

}
