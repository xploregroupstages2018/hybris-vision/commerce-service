package eu.elision.hybrisvision.hybrisvisioncommerceservice.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonUtil {

    public static JSONArray getArray(String json, String propertyName) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(json);
        return (JSONArray) jsonObject.get(propertyName);
    }
}
