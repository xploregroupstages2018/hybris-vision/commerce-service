package eu.elision.hybrisvision.hybrisvisioncommerceservice.util;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;

import java.util.List;
import java.util.stream.Collectors;

public class TagUtil {

    public static List<Tag> createParameterList(List<Tag> keywords, String tagType) {
        return keywords.stream().filter(Tag::isEnabled)
                .filter(t -> t.getType().equals(tagType)).collect(Collectors.toList());
    }
}
