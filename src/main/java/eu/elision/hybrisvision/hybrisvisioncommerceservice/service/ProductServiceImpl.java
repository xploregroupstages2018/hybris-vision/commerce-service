package eu.elision.hybrisvision.hybrisvisioncommerceservice.service;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.ProductSearch;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.locator.ProviderLocator;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.ProductProvider;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lander
 * @since 14/05/2018
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProviderLocator providerLocator;

    @Override
    public ProductSearch getProducts(Provider provider, List<Tag> keywords) {

        ProductProvider productProvider = providerLocator.getInstance(provider);

        if (productProvider != null) {
            return productProvider.getProducts(keywords);
        }

        return null;
    }
}
