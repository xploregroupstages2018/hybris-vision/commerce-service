package eu.elision.hybrisvision.hybrisvisioncommerceservice.service;

import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.ProductSearch;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.domain.Tag;
import eu.elision.hybrisvision.hybrisvisioncommerceservice.provider.Provider;

import java.util.List;

/**
 * @author Lander
 * @since 14/05/2018
 */
public interface ProductService {
    ProductSearch getProducts(Provider provider, List<Tag> keywords);
}
