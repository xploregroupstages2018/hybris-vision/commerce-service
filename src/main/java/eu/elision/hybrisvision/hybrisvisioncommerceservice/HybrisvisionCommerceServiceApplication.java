package eu.elision.hybrisvision.hybrisvisioncommerceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HybrisvisionCommerceServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HybrisvisionCommerceServiceApplication.class, args);
	}
}
