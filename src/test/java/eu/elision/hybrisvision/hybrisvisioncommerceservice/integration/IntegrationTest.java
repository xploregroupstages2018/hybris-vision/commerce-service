package eu.elision.hybrisvision.hybrisvisioncommerceservice.integration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

/**
 * @author Lander
 * @since 14/05/2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IntegrationTest {


    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    public void sendCallWithoutParameters() {
        ResponseEntity<String> response =
                restTemplate.postForEntity("/product", "", String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void sendCallWithZalandoProvider() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>("", headers);

        ResponseEntity<String> response =
                restTemplate.postForEntity("/product?provider=zalando", entity, String.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void sendCallWithWrongProvider() {
        ResponseEntity<String> response =
                restTemplate.postForEntity("/product?provider=Beau", "", String.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void sendCallWithProviderAndOneParameter() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String body = "[{\"text\":\"Adidas\",\"enabled\":\"true\",\"type\":\"label\", \"importance\":\"0.75\"}, {\"text\":\"White\",\"enabled\":\"true\",\"type\":\"color\"}]";
        HttpEntity<String> entity = new HttpEntity<>(body, headers);

        ResponseEntity<String> response =
                restTemplate.postForEntity("/product?provider=zalando", entity, String.class);

        JSONObject jsonObj = new JSONObject(response.getBody());
        JSONArray jsonArray = jsonObj.getJSONArray("products");
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(jsonArray);
    }
}
