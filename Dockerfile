FROM frolvlad/alpine-oraclejdk8
VOLUME /tmp
ADD /build/libs/hybrisvision-commerce-service-?.?.?-SNAPSHOT.jar commerce-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/commerce-service.jar"]